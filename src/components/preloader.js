import * as React from "react"

const Preloader = () => {
  const [loaded, setLoaded] = React.useState(false)
  React.useEffect(() => {
    setLoaded(true)
  })
  return (
    <div
      className={`preloader ${loaded ? "fade-out" : ""}`}
      // style={{ ...(loaded ? { display: "none" } : {}) }}
    >
      <div className="loader">
        <div className="spinner">
          <div className="double-bounce1"></div>
          <div className="double-bounce2"></div>
        </div>
      </div>
    </div>
  )
}

export default Preloader
