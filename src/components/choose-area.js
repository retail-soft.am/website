import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"
import InnovativeSvg from "../images/choose/innovative.svg"
import ExperienceSvg from "../images/choose/experience.svg"
import SupportSvg from "../images/choose/support.svg"

const ChooseArea = () => {
  return (
    <div id="choose" className="ts-choose-area pt-100">
      <div className="container">
        <div className="row justify-content-center align-items-center">
          <div className="col-lg-6 col-md-12">
            <div className="ts-choose-content">
              <span>WHY CHOOSE US</span>
              <h3>
                Expertise, Innovation and Support - The key to{" "}
                <span>your success</span>
              </h3>
              <p>
                At our company, we understand that choosing a software
                development partner is a big decision. That's why we pride
                ourselves on our commitment to delivering high-quality solutions
                that are tailored to meet the unique needs of each client. Our
                team of experts has a wide range of experience in various
                industries, and we are dedicated to providing the best results
                for our clients.
              </p>
              <div className="choose-inner-card">
                <div className="icon">
                  <img src={InnovativeSvg} alt="image" />
                </div>
                <h4>Inovations</h4>
                <p>
                  {" "}
                  We stay up-to-date with the latest technologies to provide our
                  clients with cutting-edge solutions.
                </p>
              </div>
              <div className="choose-inner-card">
                <div className="icon">
                  <img src={ExperienceSvg} alt="image" />
                </div>
                <h4>Experienced</h4>
                <p>
                  Our team of experts has a wide range of experience in various
                  industries, and we are dedicated to delivering the best
                  results for our clients.
                </p>
              </div>
              <div className="choose-inner-card">
                <div className="icon">
                  <img src={SupportSvg} alt="image" />
                </div>
                <h4>24/7 Support</h4>
                <p>
                  We provide ongoing support and maintenance to ensure that our
                  clients' software is always performing at its best.
                </p>
              </div>
            </div>
          </div>

          <div className="col-lg-6 col-md-12">
            <div className="ts-choose-image">
              <StaticImage src="../images/choose/choose.jpg" alt="image" />
            </div>
          </div>
        </div>
      </div>

      <div className="ts-choose-shape">
        <StaticImage src="../images/choose/shape.png" alt="image" />
      </div>
    </div>
  )
}

export default ChooseArea
