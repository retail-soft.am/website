import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

const Footer = () => {
  return (
    <footer className="footer-wrap-area pt-100">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 col-md-6">
            <div className="single-footer-card">
              <h4 className="logo">
                <a href="./">Retail-Soft LLC</a>
              </h4>
              <ul className="contact-info">
                <li>
                  <i className="icofont-location-pin"></i>
                  Abovyan str., 36, 0018, Yerevan, Armenia
                </li>
                <li>
                  <i className="icofont-envelope"></i>
                  <a href="mailto:mail@retail-soft.am">mail@retail-soft.am</a>
                </li>
              </ul>
              <ul className="social-icon">
                {/* <li>
                  <a href="#">
                    <i className="icofont-facebook"></i>
                  </a>
                </li> */}
                <li>
                  <a href="https://www.linkedin.com/company/llc-retail-soft/">
                    <i className="icofont-linkedin"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-lg-3 col-md-3">
            <div className="single-footer-card ps-5">
              <h3>Useful Links</h3>

              <ul className="links with-dot">
                <li>
                  <a href="#services">Services</a>
                </li>
                <li>
                  <a href="#solutions">Solutions</a>
                </li>
                <li>
                  <a href="#offer">We offer</a>
                </li>
                <li>
                  <a href="#about">About Us</a>
                </li>
                <li>
                  <a
                    href="#"
                    data-bs-toggle="modal"
                    data-bs-target="#contactForm-Modal"
                  >
                    Contact
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-lg-3 col-md-3">
            <div className="single-footer-card">
              <h3>Working Hours</h3>
              <ul className="links">
                <li>Monday - Friday: 9AM - 6PM</li>
                <li>Saturday - Sunday: Closed</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div className="copyright-wrap-area">
        <div className="container">
          <div className="inner-border">
            <div className="row justify-content-center">
              <div className="col-lg-7 col-md-7">
                <p>© 2023-{new Date().getFullYear()} Retail-Soft LLC</p>
              </div>
              <div className="col-lg-5 col-md-5">
                {/* <ul className="list">
                  <li>
                    <a href="#">Terms &amp; Conditions</a>
                  </li>
                  <li>
                    <a href="#">Privacy Policy</a>
                  </li>
                </ul> */}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="footer-wrap-shape">
        <StaticImage src="../images/footer-shape.png" alt="image" />
      </div>
    </footer>
  )
}

export default Footer
