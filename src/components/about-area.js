import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

export default class AboutArea extends React.Component {
  componentDidMount() {}
  render() {
    return (
      <div id="about" className="ts-about-area pb-100">
        <div className="container">
          <div className="row justify-content-center align-items-center">
            <div className="col-lg-6 col-md-12">
              <div className="ts-about-image">
                <StaticImage src="../images/about/about.png" alt="image" />
              </div>
            </div>

            <div className="col-lg-6 col-md-12">
              <div className="ts-about-content">
                <span>ABOUT US</span>
                <h3>
                  Expertise you can trust - <span>10 years</span> of experience
                </h3>
                <p>
                  Our company has been providing software development services
                  for over 15 years. Our team of experts has a wide range of
                  experience in various industries and is dedicated to
                  delivering high-quality solutions that meet the unique needs
                  of each client. We pride ourselves on our commitment to
                  excellence and customer satisfaction.
                </p>
                <ul className="list">
                  <li>
                    <i className="icofont-check-alt"></i> Experienced team with
                    a wide range of industry expertise
                  </li>
                  <li>
                    <i className="icofont-check-alt"></i> Tailored solutions
                    that meet the unique needs of each client
                  </li>
                  <li>
                    <i className="icofont-check-alt"></i> Commitment to
                    excellence and customer satisfaction
                  </li>
                  <li>
                    <i className="icofont-check-alt"></i> High-quality services
                    and innovative solutions
                  </li>
                </ul>
                <div className="wrap-shape">
                  <StaticImage src="../images/about/shape2.png" alt="image" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="ts-about-shape">
          <StaticImage src="../images/about/shape1.png" alt="image" />
        </div>
      </div>
    )
  }
}
