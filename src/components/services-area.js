import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

const ServicesArea = () => {
  return (
    <div id="services" className="ts-services-area pb-75">
      <div className="container">
        <div className="ts-services-inner-area pt-100">
          <div className="row justify-content-center">
            <div className="col-lg-4 col-sm-6">
              <div className="ts-services-card">
                <div className="icon">
                  <StaticImage
                    src="../images/services/technology.svg"
                    alt="image"
                  />
                </div>
                <h3>Technology Services</h3>
                <p>
                  We provide a wide range of technology services to help
                  businesses stay competitive and achieve their goals.
                </p>
              </div>
            </div>

            <div className="col-lg-4 col-sm-6">
              <div className="ts-services-card">
                <div className="icon">
                  <StaticImage
                    src="../images/services/consulting.svg"
                    quality={95}
                    alt="image"
                  />
                </div>
                <h3>Business Consulting</h3>
                <p>
                  Our consulting services help businesses optimize their
                  processes and strategies for maximum efficiency and success.
                </p>
              </div>
            </div>

            <div className="col-lg-4 col-sm-6">
              <div className="ts-services-card">
                <div className="icon">
                  <StaticImage
                    src="../images/services/design-thinking.svg"
                    quality={95}
                    alt="image"
                  />
                </div>
                <h3>Digital Solutions</h3>
                <p>
                  We offer a range of digital solutions to help businesses
                  establish a strong online presence and reach new customers.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ServicesArea
