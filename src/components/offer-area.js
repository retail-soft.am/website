import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

import Slider from "react-slick"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"

const settings = {
  autoplay: true,
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: false,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 4,
      },
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
      },
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
}

export default class OfferArea extends React.Component {
  constructor(props) {
    super(props)
    this.next = this.next.bind(this)
    this.previous = this.previous.bind(this)
  }
  next() {
    this.slider.slickNext()
  }
  previous() {
    this.slider.slickPrev()
  }
  render() {
    return (
      <div id="offer" className="ts-offer-area ptb-100">
        <div className="container">
          <div className="ts-section-title">
            <span>WHAT WE DO</span>
            <h3>What we offer for you</h3>
          </div>
          <Slider
            ref={c => (this.slider = c)}
            className="ts-offer-slides"
            {...settings}
          >
            <div className="ts-offer-card">
              <StaticImage src="../images/offer/img1.png" alt="image" />
              <h3>Software Development</h3>
              <p>
                We specialize in web and mobile application development, custom
                software development, and software maintenance and support.
              </p>
            </div>

            <div className="ts-offer-card">
              <StaticImage src="../images/offer/img2.png" alt="image" />
              <h3>Networking Integration</h3>
              <p>
                We provide integration services for networks, servers, and other
                IT systems to enhance performance and security.
              </p>
            </div>

            <div className="ts-offer-card">
              <StaticImage src="../images/offer/img3.png" alt="image" />
              <h3>IT Management Consultants</h3>
              <p>
                Our consultants provide expert advice and support to optimize
                processes, strategies and systems of businesses.
              </p>
            </div>

            <div className="ts-offer-card">
              <StaticImage src="../images/offer/img4.png" alt="image" />
              <h3>Information Security</h3>
              <p>
                We ensure the security of our client's data and software with
                regular updates and security protocols to keep up with the
                latest trends in the industry.
              </p>
            </div>

            <div className="ts-offer-card">
              <StaticImage src="../images/offer/img1.png" alt="image" />
              <h3>Software Development</h3>
              <p>
                We specialize in web and mobile application development, custom
                software development, and software maintenance and support.
              </p>
            </div>

            <div className="ts-offer-card">
              <StaticImage src="../images/offer/img2.png" alt="image" />
              <h3>Networking Integration</h3>
              <p>
                We provide integration services for networks, servers, and other
                IT systems to enhance performance and security.
              </p>
            </div>

            <div className="ts-offer-card">
              <StaticImage src="../images/offer/img3.png" alt="image" />
              <h3>IT Management Consultants</h3>
              <p>
                Our consultants provide expert advice and support to optimize
                processes, strategies and systems of businesses.
              </p>
            </div>

            <div className="ts-offer-card">
              <StaticImage src="../images/offer/img4.png" alt="image" />
              <h3>Information Security</h3>
              <p>
                We ensure the security of our client's data and software with
                regular updates and security protocols to keep up with the
                latest trends in the industry.
              </p>
            </div>
          </Slider>
          <div className="slider-nav">
            <button
              type="button"
              role="presentation"
              className="slider-prev"
              onClick={this.previous}
            >
              <i className="icofont-arrow-left"></i>
            </button>
            <button
              type="button"
              role="presentation"
              className="slider-next"
              onClick={this.next}
            >
              <i className="icofont-arrow-right"></i>
            </button>
          </div>
        </div>
      </div>
    )
  }
}
