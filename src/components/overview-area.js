import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

const OverviewArea = () => {
  return (
    <div className="ts-overview-area ptb-100">
      <div className="container">
        <div className="row justify-content-center align-items-center">
          <div className="col-lg-8 col-md-12">
            <div className="ts-overview-content">
              <h3>Join our community by using our technology services</h3>
            </div>
          </div>

          <div className="col-lg-4 col-md-12">
            <div className="ts-overview-btn">
              <a
                className="default-btn"
                href="#"
                data-bs-toggle="modal"
                data-bs-target="#contactForm-Modal"
              >
                GET STARTED
              </a>
            </div>
          </div>
        </div>
      </div>

      <div className="ts-overview-shape">
        <StaticImage src="../images/overview-shape.png" alt="image" />
      </div>
    </div>
  )
}

export default OverviewArea
