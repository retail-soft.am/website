import * as React from "react"

const MainBanner = () => {
  return (
    <div id="home" className="ts-main-banner-area">
      <div className="container-fluid">
        <div className="row align-items-center">
          <div className="col-lg-7 col-md-12">
            <div className="ts-banner-content">
              <h1>
                Empowering Your Business with Innovative Software Solutions
              </h1>
              <p>
                At our company, we specialize in providing cutting-edge software
                development services that are designed to empower your business
                and take it to the next level. Whether you need a custom
                software solution or a mobile application, our team of experts
                is here to help you achieve your goals.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default MainBanner
