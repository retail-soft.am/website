import * as React from "react"

const Navbar = () => {
  const [sticky, setSticky] = React.useState(false)

  React.useEffect(() => {
    const handleScroll = event => {
      var scrolled = window.scrollY
      if (scrolled > 170) {
        setSticky(true)
      } else {
        setSticky(false)
      }
    }

    window.addEventListener("scroll", handleScroll)

    return () => {
      window.removeEventListener("scroll", handleScroll)
    }
  }, [])
  return (
    <nav
      className={`navbar navbar-expand-lg navbar-light bg-light navbar-area-with-ts ${
        sticky ? "is-sticky" : ""
      }`}
    >
      <div className="container-fluid">
        <a className="navbar-brand" href="./">
          RETAIL-SOFT
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav m-auto">
            <li className="nav-item">
              <a className="nav-link" href="#home">
                Home
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#services">
                Services
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#about">
                About
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#offer">
                What we offer
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#choose">
                Why choose us
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link active" href="#solutions">
                Solutions
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                href="#"
                data-bs-toggle="modal"
                data-bs-target="#contactForm-Modal"
              >
                Contact
              </a>
            </li>
          </ul>

          <div className="others-option d-flex align-items-center">
            <div className="option-item">
              <a
                className="default-btn"
                href="#"
                data-bs-toggle="modal"
                data-bs-target="#contactForm-Modal"
              >
                GET A QUOTE
              </a>
            </div>
            <div className="option-item">
              <div className="info">
                <span>Free Consultation</span>
                <a href="mailto:mail@retail-soft.am">mail@retail-soft.am</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
  )
}
export default Navbar
