import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

const SolutionArea = () => {
  return (
    <div id="solutions" className="ts-solution-area">
      <div className="container-fluid">
        <div className="row justify-content-center">
          <div className="col-lg-6 col-md-12">
            <div className="ts-solution-image"></div>
          </div>

          <div className="col-lg-6 col-md-12">
            <div className="ts-solution-content">
              <span>TECHNOLOGY SOLUTION</span>
              <h3>
                We are the solution to any problem with{" "}
                <span>software development</span>
              </h3>
              <p>
                At our company, we specialize in providing innovative technology
                solutions to businesses. We have a team of experts who are
                proficient in the latest technologies and are dedicated to
                delivering high-quality solutions that meet the unique needs of
                each client. Whether you need a custom software solution or a
                mobile application, we have the expertise and experience to help
                you achieve your goals.
              </p>

              <div className="solution-inner-card">
                <h4>Expert Technical Skills</h4>
                <p>
                  <ul>
                    <li>
                      Web Development: HTML, CSS, JavaScript, GO, and more
                    </li>
                    <li>Mobile Development: Android, Flutter, and more</li>
                    <li>Embedded Development: C/C++</li>
                    <li>Cloud Computing: AWS, Azure, Google Cloud, and more</li>
                    <li>Database Management: Postgres, MSSQL, and more</li>
                  </ul>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="ts-solution-shape">
        <StaticImage src="../images/solution-shape.png" alt="image" />
      </div>
    </div>
  )
}
export default SolutionArea
