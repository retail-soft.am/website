import * as React from "react"

const GoTop = () => {
  const [visible, setVisible] = React.useState(false)

  React.useEffect(() => {
    const handleScroll = event => {
      var scrolled = window.scrollY
      if (scrolled > 300) setVisible(true)
      if (scrolled < 300) setVisible(false)
    }

    window.addEventListener("scroll", handleScroll)

    return () => {
      window.removeEventListener("scroll", handleScroll)
    }
  }, [])
  const goToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    })
  }
  return (
    <div className={`go-top ${visible ? "change" : ""}`} onClick={goToTop}>
      <i className="icofont-stylish-up"></i>
    </div>
  )
}
export default GoTop
