import * as React from "react"
import Reaptcha from "reaptcha"

export default class ContactForm extends React.Component {
  constructor(props) {
    super(props)
    this.captcha = null
    this.state = {
      verified: false,
      firstName: "",
      lastName: "",
      email: "",
      subject: "",
      message: "",
    }
  }
  onVerify = recaptchaResponse => {
    this.setState({
      verified: true,
    })
  }
  handleSubmit = event => {
    // shit
    setTimeout(() => {
      this.captcha.reset()
      this.setState({
        verified: false,
        firstName: "",
        lastName: "",
        email: "",
        subject: "",
        message: "",
      })
    }, 1000)
  }

  render() {
    return (
      <div
        className="contact-form-modal-area modal right fade"
        id="contactForm-Modal"
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
            <div className="modal-body">
              <form
                id="contactForm"
                action="https://forms.zohopublic.com/farafonovpro/form/ContactForm/formperma/AVHCDM5tPtasCECc17Fok27ZcLyCSE6wYa6LTybNX7o/htmlRecords/submit"
                name="form"
                method="POST"
                acceptCharset="UTF-8"
                encType="multipart/form-data"
                onSubmit={this.handleSubmit}
              >
                <input type="hidden" name="zf_referrer_name" value="" />
                <input type="hidden" name="zf_redirect_url" value="" />
                <input type="hidden" name="zc_gad" value="" />
                <h2>Get in Touch</h2>
                <div className="row">
                  <div className="col-lg-12 col-md-12">
                    <div className="form-group">
                      <input
                        type="text"
                        maxLength="255"
                        name="Name_First"
                        fieldtype="7"
                        id="name_first"
                        className="form-control"
                        required
                        data-error="Please enter your name"
                        placeholder="First Name"
                        value={this.state.firstName}
                        onChange={event =>
                          this.setState({ firstName: event.target.value })
                        }
                      />
                      <div className="help-block with-errors"></div>
                    </div>
                  </div>
                  <div className="col-lg-12 col-md-12">
                    <div className="form-group">
                      <input
                        type="text"
                        maxLength="255"
                        name="Name_Last"
                        fieldtype="7"
                        id="name_last"
                        className="form-control"
                        required
                        data-error="Please enter your name"
                        placeholder="Last Name"
                        value={this.state.lastName}
                        onChange={event =>
                          this.setState({ lastName: event.target.value })
                        }
                      />
                      <div className="help-block with-errors"></div>
                    </div>
                  </div>
                  <div className="col-lg-12 col-md-12">
                    <div className="form-group">
                      <input
                        type="email"
                        id="email"
                        name="Email"
                        fieldtype="9"
                        className="form-control"
                        required
                        data-error="Please enter your email"
                        placeholder="Email"
                        value={this.state.email}
                        onChange={event =>
                          this.setState({ email: event.target.value })
                        }
                      />
                      <div className="help-block with-errors"></div>
                    </div>
                  </div>
                  <div className="col-lg-12 col-md-12">
                    <div className="form-group">
                      <input
                        type="text"
                        maxLength="255"
                        name="SingleLine"
                        fieldtype="1"
                        id="msg_subject"
                        className="form-control"
                        placeholder="Subject"
                        required
                        data-error="Please enter your subject"
                        value={this.state.subject}
                        onChange={event =>
                          this.setState({ subject: event.target.value })
                        }
                      />
                      <div className="help-block with-errors"></div>
                    </div>
                  </div>
                  <div className="col-lg-12 col-md-12">
                    <div className="form-group">
                      <textarea
                        name="MultiLine"
                        maxLength="65535"
                        className="form-control"
                        id="message"
                        cols="30"
                        rows="4"
                        required
                        data-error="Write your message"
                        placeholder="Your Message"
                        value={this.state.message}
                        onChange={event =>
                          this.setState({ message: event.target.value })
                        }
                      ></textarea>
                      <div className="help-block with-errors"></div>
                    </div>
                  </div>
                  <div className="col-lg-12 col-md-12">
                    <div className="form-group">
                      <Reaptcha
                        ref={e => (this.captcha = e)}
                        sitekey="6Lfmtg8kAAAAACpLb01u60ZmuXFXTfFVnBaHd3Nr"
                        onVerify={this.onVerify}
                      />
                    </div>
                  </div>
                  <div className="col-lg-12 col-md-12">
                    <button
                      type="submit"
                      className="default-btn"
                      formTarget="_blank"
                      disabled={!this.state.verified}
                    >
                      Send
                    </button>
                    <div id="msgSubmit" className="h3 text-center hidden"></div>
                    <div className="clearfix"></div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
