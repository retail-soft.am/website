import * as React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import "../fonts/icofont/icofont.min.css"
import "../styles/layout.scss"

import Preloader from "./preloader"
import Navbar from "./navbar"
import Footer from "./footer"
import MainBanner from "./main-banner"
import ServicesArea from "./services-area"
import AboutArea from "./about-area"
import OfferArea from "./offer-area"
import GoTop from "./go-top"
import ChooseArea from "./choose-area"
import SolutionArea from "./solution-area"
import OverviewArea from "./overview-area"
import ContactForm from "./contact-form"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <div id="layout">
      {/* <main>{children}</main> */}
      <Preloader />
      <Navbar siteTitle={data.site.siteMetadata?.title || `Title`} />
      <MainBanner />
      <ServicesArea />
      <AboutArea />
      <OfferArea />
      <ChooseArea />
      <div className="ts-blog-area pt-100 pb-75">
        <div className="ts-blog-shape2">
          <StaticImage src="../images/blog-shape1.png" alt="image" />
        </div>
      </div>
      <SolutionArea />
      <div className="ts-blog-area pt-100 pb-75">
        <div className="ts-blog-shape1">
          <StaticImage src="../images/blog-shape1.png" alt="image" />
        </div>
      </div>
      <OverviewArea />
      <Footer />
      <ContactForm />
      <GoTop />
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
